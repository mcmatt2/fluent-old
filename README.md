# Fluent

This is an old router that matches URL patterns to class/method combinations (e.g. call `//host.tld/one/two` in your browser, and this calls `two()` on an instance of class `one`).

It was originally written pre-PHP 5.2 and is no longer in use (nor recommended for production environments!)

This code did exist before the initial commit; it was tightly-coupled to an older website that is no longer around (source to that can be provided upon request) and subsequently removed to a separate repository.