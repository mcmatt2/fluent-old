<?php
class FluentIO {
	private static function sanitise($file) {
		if ( strpos($file, SITE) === false ) {
			$file = SITE . '/' . $file;
		}
		$file = FluentString::replace('\\', '/', $file);
		$file = FluentString::replace('//', '/', $file);
		return $file;
	}

	/**
	 * Creates a directory on the hard drive. If a directory with the new name already exists, that existing directory will be renamed.
	 * @param string $name The new name for the directory.
	 * @return bool
	 */
	public static function createDirectory($name) {
		$name = self::sanitise($name);
		if ( self::exists($name) ) {
			self::rename($name, $name . '-' . NOW);
		}
		return mkdir($name, null, true);
	}

	/**
	 * Determines whether the given file exists on the hard drive.
	 * @param string $file The path to the file.
	 * @return bool
	 */
	public static function exists($file) {
		$file = self::sanitise($file);
		return file_exists($file);
	}

	/**
	 * Renames a file on the hard drive. If a file with the new name already exists, that existing file will be renamed.
	 * @param string $old The file to rename.
	 * @param string $new The new name for the file.
	 * @return bool
	 */
	public static function rename($old, $new) {
		$old = self::sanitise($old);
		$new = self::sanitise($new);
		if ( self::exists($new) ) {
			self::rename($new, $new . '-' . NOW);
		}
		return rename($old, $new);
	}
}
