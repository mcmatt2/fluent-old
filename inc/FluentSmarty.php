<?php
/* Layout */
function smarty_block_left($p, $c, $s, &$r) {
	if ( $c == null ) {
		$size = array_key_exists('size', $p) ? $p['size'] : 'half';
		return '<div class="float-left '.$size.'">';
	}
	return $c . '</div><!--/float-left-->';
}
function smarty_block_right($p, $c, $s, &$r) {
	if ( $c == null ) {
		$size = array_key_exists('size', $p) ? $p['size'] : 'half';
		return '<div class="float-right '.$size.'">';
	}
	return $c . '</div><!--/float-right-->';
}
function smarty_block_stretch($p, $c, $s, &$r) { if ( $c == null ) { return '<div class="stretch">'; } return $c . '</div><!--/stretch-->'; }
$_tpl->registerPlugin('block', 'left', 'smarty_block_left');
$_tpl->registerPlugin('block', 'right', 'smarty_block_right');
$_tpl->registerPlugin('block', 'stretch', 'smarty_block_stretch');


/* Tabs */
function smarty_block_tabs($p, $c, $s, &$r) { if ( $c == null ) { return '<div class="tab">'; } return $c . '</div>'; }
function smarty_block_tab($p, $c, $s, &$r) { if ( $c == null ) { $level = array_key_exists('level', $p) ? $p['level'] : 4; $t = $p['title']; $id = FluentString::sanitise($t); return '<div class="tab-section"><h' . $level . ' id="t-' . $id . '">' . $t . '</h' . $level . '>'; } return $c . '</div>'; }
$_tpl->registerPlugin('block', 'tabs', 'smarty_block_tabs');
$_tpl->registerPlugin('block', 'tab', 'smarty_block_tab');

/* Markdown */
function smarty_markdown($str) { return Markdown($str); }
$_tpl->registerPlugin('modifier', 'markdown', 'smarty_markdown');

/* UTF-8 compatibility */
function smarty_length($str) { return FluentString::length($str); }
$_tpl->registerPlugin('modifier', 'length', 'smarty_length');
function smarty_utf8($str) { return htmlentities(utf8_encode($str), ENT_COMPAT, 'utf-8'); }
$_tpl->registerPlugin('modifier', 'utf8', 'smarty_utf8');

/* Modifiers */
function smarty_possessive($str) {
	if ( $str[strlen($str) - 1] == 's' ) {
		return "{$str}'";
	}
	return "{$str}'s";
}

function smarty_aloud($str) {
	$strings = array('zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine');
	if ( count($strings) > (int) $str ) return $strings[$str];
	return $str;
}
function smarty_sprite($str) { return '<span class="sprite ' . $str . '"></span>'; }

$_tpl->registerPlugin('modifier', 'possessive', 'smarty_possessive');
$_tpl->registerPlugin('modifier', 'aloud', 'smarty_aloud');
$_tpl->registerPlugin('modifier', 'sprite', 'smarty_sprite');
