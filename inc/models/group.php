<?php
class group extends FluentModel {
	public function __construct($id) {
		parent::__construct($id, '\groups', 'permalink,id');
	}


	/**
	 * Gets the permissions that this group has access to.
	 * @return array An array containing permission objects. If no permissions are found, the array will be empty.
	 */
	public function get_permissions() {
		return $this->get_links_of_type('permission');
	}

	/**
	 * Checks if the group has access to a permission.
	 * @param mixed $permission The permission to check. This can be a string (permalink), integer (ID) or permission object.
	 * @return bool True if the group has access to the permission.
	 */
	public function has_permission($permission) {
		$permission = fluent::cast('permission', $permission);
		return $this->has_link($permission);
	}

	public function remove_permissions(/* ... */) {
		$permissions = func_get_args();
		foreach ( $permissions as $permission ) {
			if ( is_array($permission) ) { foreach ( $permission as $_p ) {
				$this->remove_permissions($_p);
			} continue; }
			$permission = fluent::cast('permission', $permission);
			$permission->remove_from_groups($this);
		}
	}

	public function add_permissions(/* ... */) {
		$permissions = func_get_args();
		foreach ( $permissions as $permission ) {
			if ( is_array($permission) ) { foreach ( $permission as $_p ) {
				$this->add_permissions($_p);
			} continue; }
			$permission = fluent::cast('permission', $permission);
			$permission->apply_to_groups($this);
		}
	}



	public function remove_users(/* ... */) {
		$users = func_get_args();
		foreach ( $users as $user ) {
			if ( is_array($user) ) { foreach ( $user as $_u ) {
				$this->remove_users($_u);
			} continue; }
			$this->delete_link($user);
		}
	}

	public function add_users(/* ... */) {
		$users = func_get_args();
		foreach ( $users as $user ) {
			if ( is_array($user) ) { foreach ( $user as $_u ) {
				$this->add_users($_u);
			} continue; }
			$this->create_link($user);
		}
	}
}
