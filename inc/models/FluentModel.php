<?php
abstract class FluentModel extends FluentObject implements iFluentModel {

#region Properties
	public static $_pdoCache = array();

	protected
		$_data = array(),
		$_data_pending = array();

	protected $_map = array();

	var $_isFluent,
		$_class,
		$_table,
		$_tableFQN,
		$_key;
#endregion



#region Construction Time Again
	public function __construct($id, $table = NULL, $key = NULL) {
		parent::__construct();

		$this->_isFluent = ( strpos($table, '\\') !== FALSE );
		$this->_class = get_called_class();
		$this->_table = $table;
		$this->_tableFQN = ( $this->_isFluent ? $this->_db->_prefix_fluent : $this->_db->_prefix ) . str_replace('\\', null, $this->_table);
		$this->_key = is_array($key) ? $key : FluentString::explode(',', $key);

		if ( !array_key_exists($this->_table, self::$_pdoCache) ) {
			$_q = "SELECT * FROM `{$this->_tableFQN}` WHERE ";
			$_where = array();
			foreach ( $this->_key as $_k ) { $_where[] = "$_k = :id"; }
			$_q .= implode(' OR ', $_where) . ' LIMIT 1';
			self::$_pdoCache[$this->_table] = $this->_db->prepare($_q);
		}
		$stmt =& self::$_pdoCache[$this->_table];
		$stmt->execute(array(':id' => $id));
		$data = $stmt->fetch();

		if ( !$data || count($data) <= 0 ) {
			// none found
			return;
		}

		foreach ( $data as $_k => $_v ) {
			if ( is_int($_k) || !$this->_has_field($_k) ) {
				continue;
			}
			if ( array_key_exists($_k, $this->_map) && class_exists($this->_map[$_k]) ) {
				$func = $this->_map[$_k];
				$this->_data[$_k] = is_object($_v) ? $_v : $func($_v);
				continue;
			}
			if ( class_exists($_k, false) ) {
				$this->_data[$_k] = $_k($_v);
				continue;
			}
			$this->_data[$_k] = $_v;
		}

		foreach ( $this->_key as $_k ) {
			self::$_cache[$this->_class][$this->$_k] =& $this;
		}
	}

	/**
	 * Returns TRUE if the table attached to this object contains the specified field.
	 * @param string  $field The field to check.
	 * @return boolean
	 */
	private function _has_field($field = NULL) {
		return $this->_db->table_has_field($this->_table, $field);
	}

	private static function __get_static_data() {
		$_c = get_called_class();
		$_c = $_c(null);
		$return['class'] = $_c->_class;
		$return['fluent'] = $_c->_isFluent;
		$return['table'] = $_c->_table;
		$return['tableFQN'] = $_c->_tableFQN;
		return (object) $return;
	}
#endregion



#region Overloading

	/**
	 * Gets the value of a field. If the value can be mapped to an existing object, an instance of that object will be returned. Otherwise, a string value is returned.
	 * @param string $field The name of the field to get the value of.
	 * @return mixed
	 */
	public function __get($field) {
		if ( !$field ) {
			return NULL;
		}

		$flbm = '!flbm!';
		/* "!flbm!" 'fluent_bypass_methods' - if the $field contains this
			string, don't attempt to run any dynamic get_/is_ methods. Then
			remove the !flbm! string from the $field and continue. This is
			required in some cases to prevent infinite looping. */
		if ( substr($field, 0, strlen($flbm)) !== $flbm ) {
			$new_field = $flbm . $field;

			$method = 'get_' . $field;
			if ( method_exists($this, $method) ) {
				return $this->$method($this->_has_field($field) ? $this->$new_field : NULL);
			}

			$method = 'is_' . $field;
			if ( method_exists($this, $method) ) {
				return $this->$method($this->_has_field($field) ? $this->$new_field : NULL);
			}
		} else {
			$field = substr($field, strlen($flbm));
		}

		if ( array_key_exists($field, $this->_data_pending) ) {
			return $this->_data_pending[$field];
		}
		if ( array_key_exists($field, $this->_data) ) {
			return $this->_data[$field];
		}

		return parent::__get($field);
	}

	/**
	 * Sets the value of a field. Attempting to set the ID of an object will result in an exception, as will attempting to set a field which does not exist.
	 * 
	 * Unless you call $this->commit(), the values you set will only be available for the duration of this script's execution. In other words, the data is not stored in the database until you call that method.
	 * @param string $field The name of the field to set the value of.
	 * @param mixed $val The value to set for the field.
	 */
	public function __set($field, $val) {
		if ( !$this->_has_field($field) ) {
			throw new Exception('The "' . $this->_tableFQN . '" table does not have a field named "' . $field . '".');
			return;
		}
		if ( $field == 'id' ) {
			throw new Exception('An object\'s identifier cannot be altered programmatically.');
			return;
		}

		$this->__call_if_exists('on_' . $field . '_changing', array($this->$field, $val));

		if ( class_exists($field, false) ) {
			$this->_data_pending[$field] = is_object($val) ? $val : $field($val);
			$this->__call_if_exists('on_' . $field . '_changed', array($this->$field, $val));
			return;
		}

		if ( $val !== NULL && array_key_exists($field, $this->_map) && class_exists($this->_map[$field]) ) {
			$func = $this->_map[$field];
			$this->_data_pending[$field] = is_object($val) ? $val : $func($val);
			$this->__call_if_exists('on_' . $field . '_changed', array($this->$field, $val));
			return;
		}

		$this->_data_pending[$field] = $val;
		$this->__call_if_exists('on_' . $field . '_changed', array($this->$field, $val));
		return;
	}

	public function __isset($field) {
		return $this->_has_field($field) && ( $this->_data_pending[$field] != NULL || $this->_data[$field] != NULL );
	}

	public function __unset($field) {
		if ( !$this->_has_field($field) ) {
			throw new Exception('The "' . $this->_tableFQN . '" table does not have a field named "' . $field . '".');
			return;
		}
		if ( $field == 'id' ) {
			throw new Exception('An object\'s identifier cannot be altered programmatically.');
			return;
		}

		$this->$field = NULL;
	}

	public function __call($method, $arguments) {
		if ( substr($method, 0, 4) === 'get_' ) {
			$field = substr($method, 4);
			return $this->$field;
		}
		if ( substr($method, 0, 4) === 'set_' ) {
			$field = substr($method, 4);
			$this->$field = $arguments[0];
			return $this;
		}
		throw new Exception('The class "' . $this->_class . '" does not contain a method named "' . $method . '", and no suitable overload could be found.');
	}

	public function __call_if_exists($method, $arguments) {
		if ( method_exists($this, $method) ) {
			return $this->$method($arguments);
		}
		return NULL;
	}

	public function __toString() {
		$str = "\n" . '{'. "\n";
		$bits = array();
		foreach ( $this->_data as $_k => $_v ) {
			$bits[] = "\t" . $_k . ': "' . $this->$_k . '"';
		}
		$str .= implode("\n", $bits);
		$str .= "\n" . '}';
		return $str;
	}
#endregion



#region Persistence

	/**
	 * @see iFluentModel::commit()
	 */
	public function commit() {
		$_q = "UPDATE `{$this->_tableFQN}` SET ";
		$_fields = array();
		foreach ( $this->_data_pending as $_k => &$_v ) {
			$_fields[] = "`{$_k}` = :{$_k}";
		}
		$_q .= trim(implode(', ', $_fields)) . ' WHERE `id` = :id LIMIT 1';

		$_values = array(':id' => $this->id);
		foreach ( $this->_data_pending as $_k => &$_v ) {
			$_values[':' . $_k] = ( $_v instanceof self ? $_v->id : $_v );
		}

		$stmt = $this->_db->prepare($_q);

		$this->_db->unsilence();
		try {
			$stmt->execute($_values);
		} catch(Exception $e) {
			$this->_db->silence();
			return FALSE;
		}
		$this->_db->unsilence();

		foreach ( $this->_data_pending as $_k => &$_v ) {
			$this->__call_if_exists('on_' . $_k . '_committed', $_v);
		}

		$this->_data = array_merge($this->_data, $this->_data_pending);
		$this->_data_pending = array();

		return TRUE;
	}

	/**
	 * @see iFluentModel::rollback()
	 */
	public function rollback() {
		$this->_data_pending = array();
		return $this;
	}

	/**
	 * @see iFluentModel::create()
	 */
	public static function create($vars) {
		global $_db;
		$_data = self::__get_static_data();
		$class = $_data->class;
		$table = $_data->table;
		$tableFQN = $_data->tableFQN;
		$fields = array();
		$table_fields = $_db->get_table_fields($table);
		foreach ( $table_fields as $_f ) {
			$fields[$_f] = NULL;
		}

		$fields = array_merge($fields, $vars);

		$sql = 'INSERT INTO `' . $tableFQN . '` (';
		$_ = array();
		foreach ( $fields as $_f => $_v ) {
			$_[] = '`' . $_f . '`';
		}
		$sql .= implode(',', $_) . ') VALUES (';
		$_ = array();
		foreach ( $fields as $_f => $_v ) {
			if ( $_v == NULL ) {
				$_[] = 'DEFAULT';
			} else {
				$_[] = ':' . $_f;
			}
		}
		$sql .= implode(',', $_) . ')';
		unset($_);

		$prep = $_db->prepare($sql);
		$parameters = array();

		foreach ( $fields as $_f => $_v ) {
			if ( !in_array($_f, $table_fields) ) continue;
			if ( $_v == NULL ) continue;

			if ( $_v instanceof self ) {
				$parameters[':' . $_f] = $_v->id;
			} else {
				$parameters[':' . $_f] = $_v;
			}
		}

		$_db->beginTransaction();
		$_db->unsilence();
		try {
			$prep->execute($parameters);
		} catch(Exception $e) {
			$_db->silence();
			$_db->rollBack();
			return FALSE;
		}

		$id = $_db->lastInsertID();
		$_db->commit();
		$_db->silence();

		return $id ? $class($id) : FALSE;
	}

	/**
	 * TODO.
	 * @see iFluentModel::delete()
	 */
	public static function delete($fields = NULL, $values = NULL) {
		return FALSE;
	}

	/**
	 * TODO.
	 * Also, deprecate this.
	 */
	public function update_field($field, $value) { /*$this->$field = $value;*/ }
#endregion



#region Relationships

	public function has_link($obj1 = NULL) {
		return $this->_db->has_link($this, $obj1);
	}
	public function get_links_of_type($type = NULL) {
		return $this->_db->get_links_of_type($this, $type);
	}
	public function create_link($obj = NULL) {
		return $this->_db->create_link($this, $obj);
	}
	public function delete_link($obj = NULL) {
		return $this->_db->delete_link(this, $obj);
	}
#endregion



#region Calling objects through functions
	public static $_cache = array();
	public static function & get($id = NULL) {
		$class = get_called_class();

		if ( $id instanceof self ) {
			return $id;
		}

		if ( @isset(self::$_cache[$class][$id]) ) {
			return self::$_cache[$class][$id];
		}
		if ( !array_key_exists($class, self::$_cache) ) {
			self::$_cache[$class] = array();
			ksort(self::$_cache);
		}
		$obj = new $class($id);
		self::$_cache[$class][$obj->id] = $obj;
		if ( $obj->path != '' ) {
			self::$_cache[$class][$obj->path] = self::$_cache[$class][$obj->id];
		}
		return $obj;
	}
#endregion

	/**
	 * Prints a human-readable representation of this object.
	 * @param boolean $preformatted If true, the output will be wrapped in <pre> tags.
	 * @param boolean $recursive If true, child objects will also have their dump() methods called.
	 * @param integer $indent Internal parameter used with recursion.
	 */
	public function dump($preformatted = TRUE, $recursive = TRUE, $indent = 0) {
		if ( !DEBUG ) return;

		$cvar = '#8f8';
		$ccomment = '#75715e';
		$cobj = '#66d9ef';//f92672
		$cconst = '#ae81ff';
		$cstr = '#e6DB74';

		$echo = '';

		if ( $preformatted ) {
			$echo .= '<pre style="background:#222222;border:1px solid #000;aborder-radius:8px;box-shadow:0 0 8px rgba(0, 0, 0, 0.5);color:#f8f8f2;amargin:-8px -8px -13px -8px;padding:8px;atext-shadow:1px 1px 2px #000;overflow-x:auto;">';
		}

		$echo .= "(<b style='color:$cobj'>$this->_class</b>) { \n";
		$fields = array();
		foreach ( $this->_data as $_k => $_v ) {
			$modified = array_key_exists($_k, $this->_data_pending);

			$value = $modified ? $this->_data_pending[$_k] : $this->_data[$_k];

			$_ = "<span style='color:$cvar'>$_k</span>";
			if ( $modified ) {
				$_ = "<span style='background:#f00'>$_</span>";
			}
			$_ = str_repeat("\t", $indent + 1) . $_ . ': ';

			if ( $value instanceof self ) {
				if ( !$value->id ) {
					$fields[] = "$_(<b style='color:$cobj'>$value->_class</b>) <b style='color:$cconst'>NULL</b>";
				} else {
					if ( $recursive && $indent < 3 ) {
						$fields[] = $_.$value->dump(FALSE, $recursive, $indent + 1);
					} else {
						$fields[] = "$_(<b style='color:$cobj'>$value->_class</b>) <b style='color:$cconst'>$value->id</b>";
					}
				}
			} else if ( is_object($value) ) {
				$type = get_class($value);
				if ( $type === 'stdClass' ) {
					$type = 'object';
				}
				$fields[] = "$_(<b style='color:$ccomment'>$type</b>)";
			} else if ( is_bool($value) ) {
				$value = $value ? 'true' : 'false';
				$fields[] = "$_(bool) $value";
			} else if ( ((int) $value) > 0 ) {
				$fields[] = "$_<b style='color:$cconst'>$value</b>";
			} else if ( is_string($value) ) {
				$value = str_replace('"', '\\"', htmlentities($value));
				$fields[] = "$_<span style='color:$cstr'>\"$value\"</span>";
			} else if ( is_null($value) ) {
				$fields[] = "$_<b style='color:$cconst'>NULL</b>";
			}
		}

		$echo .= implode(','."\n", $fields) . "\n" . str_repeat("\t", $indent). '}';

		if ( $preformatted ) {
			$echo .= '</pre>';
		}

		return $echo;
	}

#region Retrieval

	/**
	 * Returns an array containing every object of this type which matches your criteria. If you don't pass any criteria, every object will be returned. If no objects are found, an empty array is returned.
	 * @param string $fields This forms the "WHERE" part of the SQL query.
	 * @param mixed $values This is an array (or string, which is turned into an array) of $field => $value pairs.
	 * @param string $sort This forms the "ORDER BY" part of the SQL query.
	 * @param int $limit  [description]
	 * @return array
	 */
	public static function get_all($fields = NULL, $values = NULL, $sort = NULL, $limit = NULL) {
		$data = self::__get_static_data();
		$class = $data->class;
		$table = $data->tableFQN;

		$_q = sprintf('SELECT `id` FROM `%s`', $table);

		if ( $fields )
			$_q .= ' WHERE ' . $fields;

		if ( $sort )
			$_q .= ' ORDER BY ' . $sort;

		if ( $limit )
			$_q .= ' LIMIT ' . $limit;

		if ( $values !== NULL && !is_array($values) ) {
			$values = array($values);
		}

		if ( is_array($values) ) {
			$_count = count($values);
			for ( $i = 0; $i < $_count; $i++ ) {
				if ( $values[$i] instanceof self ) {
					$values[$i] = $values[$i]->id;
				}
			}
		}

		global $_db;
		$return = array();
		foreach ( $_db->query($_q, $values) as $_obj ) {
			$return[] = $class($_obj[0]);
		}
		return $return;
	}

	/**
	 * Similar to self::get_all(), this method returns a single object of this type which matches your criteria (if any). If an object matching this criteria is not found, NULL is returned.
	 * @param string $fields This forms the "WHERE" part of the SQL query.
	 * @param mixed $values This is an array (or string, which is turned into an array) of $field => $value pairs.
	 * @return object
	 */
	public static function get_one($fields = NULL, $values = NULL) {
		$object = self::get_all($fields, $values, NULL, 1);

		if ( $object ) {
			return $object[0];
		}
		return NULL;
	}

	/**
	 * TODO: document this.
	 */
	public static function get_count($fields = NULL, $values = NULL) {
		$data = self::__get_static_data();
		$class = $data->class;
		$table = $data->tableFQN;

		$_q = $fields
			  ? 'SELECT COUNT(*) FROM `' . $table . '` WHERE ' . $fields
			  : 'SELECT COUNT(*) FROM `' . $table . '`';

		if ( $values !== NULL && !is_array($values) ) {
			$values = array($values);
		}

		if ( is_array($values) ) {
			$_count = count($values);
			for ( $i = 0; $i < $_count; $i++ ) {
				if ( $values[$i] instanceof self ) {
					$values[$i] = $values[$i]->id;
				}
			}
		}

		global $_db;
		$_q = $_db->query($_q, $values);
		return @$_q[0][0];
	}
#endregion

}
