<?php
class user extends FluentModel {
	public function __construct($id) {
		parent::__construct($id, '\users', 'username,id');
	}

	public function __toString() {
		return $this->name;
	}

	/**
	 * Returns this user's nickname, or their username if they don't have a nickname. Use $user->name to access it.
	 * @return string
	 */
	protected function get_name() {
		return coalesce($this->nickname, $this->username);
	}

	/**
	 * Returns this user's avatar. Use $user->avatar to access it.
	 * @return string
	 */
	protected function get_avatar() {
		$anon = 'images/anonymous.png';
		if ( !$this->id ) {
			return $anon;
		}

		$file = SITE . '/images/avatars/' . $this->username . '.png';
		if ( file_exists($file) ) {
			return FluentString::replace(SITE, URL, $file);
		}
		return '//gravatar.com/avatar/' . md5($this->email) . '?r=r&amp;d=' . urlencode(URL . '/' . $anon);
	}

	/**
	 * Gets the groups that this user belongs to.
	 * @return array An array containing group objects. If no groups are found, the return value will be null.
	 */
	public function get_groups() {
		return $this->get_links_of_type('group');
	}

	/**
	 * Gets the permissions that this user has.
	 * @return array An array containing permission objects. If no permissions are found, the return value will be null.
	 */
	public function get_permissions() {
		$groups = $this->get_groups();
		$permissions = array();

		foreach ( $groups as $_g ) {
			foreach ( $_g->get_links_of_type('permission') as $_p ) {
				$permissions[] = $_p;
			}
		}

		return $permissions;
	}

	/**
	 * Checks if the user belongs to a specified group.
	 * @param mixed $group The group to check. This can be a string (permalink), integer (ID) or group object.
	 * @return bool True if the user is in the group.
	 */
	public function in_group($group) {
		$group = fluent::cast('group', $group);
		return $this->has_link($group);
	}

	/**
	 * Checks if the user belongs to all of the groups passed.
	 * @param array $groups An array containing groups to check.
	 * @return bool True if the user is in <b>every</b> group passed.
	 */
	public function in_all_groups($groups) {
		foreach ( $groups as $group ) {
			if ( !$this->in_group($group) ) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Checks if the user belongs to at least one of the groups passed.
	 * @param array $groups An array containg groups to check.
	 * @return bool True if the user is in <b>at least one</b> group passed.
	 */
	public function in_any_group($groups) {
		foreach ( $groups as $group ) {
			if ( $this->in_group($group) ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks if the user has a specified permission.
	 * @param mixed $permission The permission to check. This can be a string (permalink), integer (ID) or permission object.
	 * @return bool True if the user has that permission.
	 */
	public function has_permission($permission) {
		$permission = fluent::cast('permission', $permission);
		foreach ( $this->get_groups() as $group ) {
			if ( $group->has_permission($permission) ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks if the user has at least one passed permission.
	 * @param array $permissions An array containing permissions to check.
	 * @return bool True if the user has <b>at least one</b> permission passed.
	 */
	public function has_any_permission($permissions) {
		$permissions = fluent::to_array($permissions);
		foreach ( $permissions as $permission ) {
			if ( $this->has_permission($permission) ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks if the user has all passed permissions.
	 * @param array $permissions An array containing permissions to check.
	 * @return bool True if the user has <b>all</b> permissions passed.
	 */
	public function has_all_permissions($permissions) {
		$permissions = fluent::to_array($permissions);
		foreach ( $permissions as $permission ) {
			if ( !$this->has_permission($permission) ) {
				return false;
			}
		}
		return true;
	}

	public static function hash_password($pass) {
		if ( strlen($pass) != 40 ) {
			return sha1($pass);
		}
		return $pass;
	}

	public static function account_exists($user, $pass) {
		$account = self::get_one('`username` = ? AND `password` = ?', array($user, self::hash_password($pass)));
		return $account && $account->id > 0;
	}

	/**
	 * Retrieve a user's ID from their username (or 0 if the user was not found.)
	 * @static
	 * @param string $user The user's username.
	 * @return int
	 */
	public static function get_id_from_username($user) {
		$account = self::get_one('`username` = ?', array($user));
		if ( $account && $account->id ) {
			return $account->id;
		}
		return 0;
	}

	/**
	 * Retrieve a user's ID from their username and password (or 0 if the user was not found.)
	 * @static
	 * @param string $user The user's username.
	 * @param string $pass The user's password. This should be hashed with the SHA-1 algorithm
	 * @return int
	 */
	public static function get_id_from_credentials($user, $pass) {
		$account = self::get_one('`username` = ? AND `password` = ?', array($user, self::hash_password($pass)));
		if ( $account && $account->id ) {
			return $account->id;
		}
		return 0;
	}

	/**
	 * Retrieve a user's username and hashed password from their user ID. For obvious reasons, use extremely sparingly.
	 * If the account exists, an object will be returned containing 'username' and 'password' properties (null otherwise.)
	 * @static
	 * @param int $id The user's ID.
	 * @return object
	 */
	public static function get_credentials_from_id($id) {
		$account = self::get_one('`id` = ?', array($id));
		if ( $account && $account->id ) {
			return (object) array('username' => $account->username, 'password' => $account->password);
		}
		return null;
	}

	public static function get_id_from_thirdparty($remote_id) {
		global $_db;
		$sql = $_db->query('SELECT `user` FROM `___\users_thirdparty` WHERE `id` = :id LIMIT 1', array(':id' => $remote_id));
		if ( count($sql) > 0 && array_key_exists('user', $sql[0]) ) {
			$user = user($sql[0]['user']);
			return $user ? $user->id : 0;
		}
		return 0;
	}
}
