<?php
class permission extends FluentModel {
	public function __construct($id) {
		parent::__construct($id, '\permissions', 'permalink,id');
	}

	public function remove_from_groups(/* ... */) {
		$groups = func_get_args();
		foreach ( $groups as $group ) {
			if ( is_array($group) ) { foreach ( $group as $_g ) {
				$this->remove_from_groups($_g);
			} continue; }
			$this->delete_link($group);
		}
	}

	public function apply_to_groups(/* ... */) {
		$groups = func_get_args();
		foreach ( $groups as $group ) {
			if ( is_array($group) ) { foreach ( $group as $_g ) {
				$this->apply_to_groups($_g);
			} continue; }
			$this->create_link($group);
		}
	}
}
