<?php
/**
 * @author Matt McMahon
 * @description Describes the methods that a model should implement for maximum compatibility.
 */
interface iFluentModel {

	/* Overloading */

	/**
	 * Retrieve a field's value using $this->$field.
	 * 
	 * This method should also check if methods named "get_$field" or "is_$field" exist, and if so, route the request to those methods.
	 * @param string $field The field to retrieve the value of.
	 * @return mixed
	 */
	public function __get($field);

	/**
	 * Set the value of a field using $this->$field = $value.
	 * 
	 * Attempts to set either the "id" field or a field that doesn't exist in the current table should throw an exception.
	 * 
	 * If a class named $field exists, then $field should be stored as an instance of that class with $value as the parameter.
	 * 
	 * This method should also trigger two methods, if they exist: "on_$field_changing", before the value is set, and "on_$field_changed" after the value is set.
	 * 
	 * Note: the changes should NOT be committed to the database automatically. The user should call the commit() method, which saves all changes (or rollback() to undo the changes).
	 * @param string $field The field to set the value of.
	 * @param mixed $value The value to set the field to.
	 */
	public function __set($field, $value);

	/**
	 * Returns true if the current table contains the $field and the value of it is not NULL.
	 * @param string $field The field to check the value of.
	 * @return boolean
	 */
	public function __isset($field);

	/**
	 * This is equivalent to calling $this->__set($field, NULL) (or, alternatively, $this->$field = NULL.)
	 * @param string $field The field to unset the value of.
	 */
	public function __unset($field);

	/**
	 * This should check for "get_$field" and "set_$field". If either of these are called, return $this->$field (or set $this->$field = $arguments[0].)
	 * 
	 * If neither of these methods were called, throw an exception.
	 * @param string $method The name of the method which was called from another point in the code.
	 * @param array $arguments An array containing arguments which were passed to the method.
	 * @return [type]
	 */
	public function __call($method, $arguments);

	/**
	 * Returns a friendly-formatted version of this object, used for when code such as 'echo $this;' is executed.
	 * @return string
	 */
	public function __toString();



	/* Persistence */

	/**
	 * Saves any changes the user has made to this object through $this->$field = $value.
	 * 
	 * For each modified property, this method should trigger "on_$field_committed" after the commit is performed successfully. It should not be triggered if the commit fails.
	 * 
	 * This method returns TRUE if the commit was successful; FALSE otherwise.
	 * @return boolean
	 */
	public function commit();

	/**
	 * Rolls back any changes the user has made to this object, restoring it to its unaltered, fresh-from-the-source state.
	 * 
	 * This method should return $this, to allow for chaining.
	 * @return self
	 */
	public function rollback();

	/**
	 * Creates a new instance of this object. This returns several types, depending on the outcome of the method:
	 * 
	 * If creation was successful, an instance of FluentModel will be returned representing the newly-inserted row in the data store.
	 * 
	 * If the creation was unsuccessful, boolean FALSE will be returned.
	 * @param array $vars A $field=>$value array containing the data that should be inserted into the data store.
	 * @param boolean $return_instance If true, a new instance of FluentModel will be returned (if the creation was successful). if false, the ID of the created object will be returned instead.
	 * @return mixed
	 */
	public static function create($vars);

	/**
	 * Deletes an instance of this object. Depending on the context in which this method is called, one or more objects may be deleted.
	 * 
	 * If delete() is called statically, the $fields and $values parameters determine which objects are deleted.
	 * 
	 * If delete() is called non-statically, the object that the current instance of FluentModel represents will be deleted. That is, if you call $user->delete(), that user alone will be deleted.
	 * 
	 * Returns TRUE if the object was successfull deleted; FALSE otherwise.
	 * @param string $fields A list of fields to search through.
	 * @param array $values A $field=>$value array of values that each field should match.
	 * @return boolean
	 */
	public static function delete($fields = NULL, $values = NULL);



	/* Relationships */

	/**
	 * Returns true if this object has a link to the passed object.
	 * @param fluent_object $obj1 The other object to compare against.
	 * @return boolean
	 */
	public function has_link($obj1);

	/**
	 * TODO: document this.
	 */
	public function get_links_of_type($type);

	/**
	 * Creates a link from this object to the passed object. Returns true if the creation was successful, or if the objects were already releated in the first place (essentially created.)
	 * @param fluent_object $object The other object to create a link to.
	 * @return boolean
	 */
	public function create_link($object);

	/**
	 * Deletes a link to this object from the passed object. Returns true if the deletion was successful, or if the objects weren't related in the first place (essentially deleted.)
	 * @param fluent_object $object The other object to delete a link from.
	 * @return boolean
	 */
	public function delete_link($object);



	/* Retrieval */

	/**
	 * Returns an array containing every object of this type which matches the user's criteria. If no criteria is passed, every object of this type will be returned. If no objects are found, an empty array is returned.
	 * @param string $fields A list of fields to search through.
	 * @param mixed $values A $field=>$value array of values that each field should match.
	 * @param string $sort The order in which to return objects.
	 * @param int $limit A limit on the number of objects which should be returned.
	 * @return array
	 */
	public static function get_all($fields = NULL, $values = NULL, $sort = NULL, $limit = NULL);

	/**
	 * Returns a single object of this type which matches the user's criteria (if any). If no object is found, NULL is returned.
	 * @param string $fields A list of fields to search through.
	 * @param mixed $values A $field=>$value array of values that each field should match.
	 * @return FluentModel
	 */
	public static function get_one($fields = NULL, $values = NULL);

	/**
	 * Returns a count of the total number of objects of this type which matches the user's criteria (if any).
	 * @param string $fields A list of fields to search through.
	 * @param mixed $values A $field=>$value array of values that each field should match.
	 * @return int
	 */
	public static function get_count($fields = NULL, $values = NULL);
}
