<?php
class FluentTemplate {
	var $_engine, $_type;

	public function __construct() {
		$smarty = FLUENT_INCLUDES . '/thirdparty/smarty/Smarty.class.php';
		$twig = FLUENT_INCLUDES . '/thirdparty/Twig/Autoloader.php';

		if ( file_exists($smarty) ) {
			require_once($smarty);
			$this->_engine = new Smarty();
			$this->_engine->setCacheDir(SITE . "/cache/templates");
			$this->_engine->setCompileDir(SITE . "/cache/templates");
			$this->_engine->debugging = false;
			$this->_engine->caching = false;
			$this->_type = 'smarty';
			$_tpl = &$this;
			require_once(FLUENT_INCLUDES . '/FluentSmarty.php');
		}
		else if ( file_exists($twig) ) {
			require_once($twig);
			Twig_Autoloader::register();
			$this->_engine = new Twig_Environment(new Twig_Loader_Filesystem(SITE), array(
				'cache' => SITE . '/cache/templates',
				'debug' => true,
				'strict_variables' => true,
			));
			$this->_type = 'twig';
		}
		else {
			throw new Exception('Fluent requires a template engine. Install Twig or Smarty as directed in the user guide, and reload this page.');
		}
	}

	public function assign($var, $val = null) {
		if ( is_array($var) ) {
			foreach ( $var as $_k => $_v ) {
				$this->assign($_k, $_v);
			}
		} else {
			if ( $this->_type == 'smarty' ) {
				$this->_engine->assign($var, $val);
			} else if ( $this->_type == 'twig' ) {
				$this->_engine->addGlobal($var, $val);
			}
		}
	}

	/**
	 * Instruct the template engine to display a template.
	 * @param string $tpl The template to display.
	 * @param array $vars An optional array containing $variable => $value pairs to be assiged to the template engine.
	 */
	public function display($tpl, $vars = null) {
		if ( substr($tpl, 0, 1) === '\\' ) {
			$tpl = FLUENT_VIEWS . '/' . substr($tpl, 1);
		} else {
			$tpl = SITE_VIEWS . '/' . $tpl;
		}

		$tpl = $tpl . '.tpl';

		if ( $this->_type == 'smarty' ) {
			if ( is_array($vars) && count($vars) > 0 ) {
				foreach ( $vars as $k => $v ) {
					$this->assign($k, $v);
				}
			}
			$this->_engine->display($tpl);
		}
		else if ( $this->_type == 'twig' ) {
			echo $this->_engine->render($tpl, is_array($vars) ? $vars : array());
		}
	}

	public function registerPlugin($type, $name, $func) {
		$this->_engine->registerPlugin($type, $name, $func);
	}
}
