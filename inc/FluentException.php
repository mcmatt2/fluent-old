<?php
set_exception_handler('fluent_exception');
function fluent_exception($e) {
	ob_end_clean();
	header('HTTP/1.0 500 Internal Server Error', true, 500);
	header('Content-Type: text/html; charset=utf-8');
	$site = 'Fluent';
	$title = $site . ' Error (500)';
	$ex = file_exists('../.debug') ? $e : NULL;
?>
<!DOCTYPE html>
<html>
<head>
<title><?=$title?></title>
<style type="text/css">
* { -moz-box-sizing: border-box; -webkit-box-sizing: border-box; -ms-box-sizing: border-box; box-sizing: border-box; margin: 0; padding: 0; }
body {
	color: #333;
	cursor: default;
	font-family: sans-serif;
	font-size: 0.9em;
	padding: 16px 16px 0 16px;
}
h1, h2, hr, p, pre, table { margin-bottom: 16px; }
hr {
	background: #999;
	color: #999;
	border: none;
	height: 1px;
}
pre {
	background: #EEE;
	border: 1px solid #999;
	font-family: monospace;
	padding: 8px;
}
code {
	display: block;
}
code code {
	background: #EEE;
	border: 1px solid #999;
	border-radius: 4px;
	display: inline-block;
	padding: 2px 0;
}
code b {
	background: #CCC;
	border-right: 1px solid #999;
	border-bottom-left-radius: 2px;
	border-top-left-radius: 2px;
	padding: 2px 6px;
}
code i { font-style: normal; padding: 2px 6px; }
table, td, th { border: 1px solid #999; border-spacing: 0; }
table { border-width: 0 1px 1px 0; color: #000; width: 100%; }
table th { text-align: left; }
td, th { border-width: 1px 0 0 1px; padding: 4px; }
th { background: #EEE; }
</style>
</head>
<body>
	<h1><?=$title?></h1>
	<p>There's something wrong with <?=$site?>; an error occurred which prevented the site from completing your request.</p>
<?php if ( @$ex ): ?>

	<hr />

	<h2>Message</h2>
	<pre><?=$e->getMessage()?></pre>

	<h2>Trace</h2>
	<table>
		<tr>
			<th>#</th>
			<th>File</th>
			<th>Line</th>
			<th>Method Call</th>
		</tr>
<?php $i = count($ex->getTrace()) - 1; foreach ( $ex->getTrace() as $trace ): ?>
		<tr>
			<td><?=$i?></td>
			<td><?=array_key_exists('file', $trace) ? $trace['file'] : '...'?></td>
			<td><?=array_key_exists('line', $trace) ? $trace['line'] : '...'?></td>
			<td><code><?=( array_key_exists('class', $trace) ? ( $trace['class'] ). '::' : '' ) . $trace['function']?> ( <?php $j = 1; $out = ''; if ( array_key_exists('args', $trace) ) { foreach($trace['args'] as $arg) { if ( $arg == null ) { continue; } $out .= '<code><b>' . $j . '</b><i>' . ( is_string($arg) ? htmlentities($arg) : '(object)' ) . '</i></code> '; $j++; } } echo rtrim(rtrim($out), ','); ?> );</code></td>
		</tr>
<?php $i--; endforeach; ?>
	</table>

<?php else: ?>
	<p>The developers have been made aware of the error. Please try again later.</p>
<?php endif; ?>
</body>
</html>
<?php
	die();
}
