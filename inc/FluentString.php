<?php
class FluentString {
	const MULTI_REPLACE_DELIMITER = '{FLUENT_MRD}';

	const HTML_ENTITY_SUFFIXES = 'acute;,cedil;,circ;,elig;,grave;,ordf;,rdm;,ring;,slash;,tilde;,uml;';
	const HTML_ENTITY_SUFFIXES_SAFE = '!!acute;!!,!!cedil;!!,!!circ;!!,!!elig;!!,!!grave;!!,!!ordf;!!,!!rdm;!!,!!ring;!!,!!slash;!!,!!tilde;!!,!!uml;!!';
	const SANITISATION_CHARACTERS = '!"£$%^&*()[]{}=_+;\'#:@~`¬¦|\\,./<>?';

	/**
	 * Extends the native PHP explode function with the following features:
	 * 1) If 'delimiter' is an array, the string will be exploded using all elements of that array as a delimeter.
	 * 2) Empty elements will be removed from the resulting array.
	 * 
	 * Performs the native PHP explode function on the $string, removes empty elements from the resulting array, and then returns that array. If the $delimiter parameter is an empty string, explode() returns null.
	 * @param mixed $delimiter The delimiter; the string used as a "separator" between array elements. This can also be an array of strings.
	 * @param string $string (Optional) The string to explode.
	 * @return array
	 */
	public static function explode($delimiter, $string = null) {
		if ( $delimiter == '' ) {
			return null;
		}

		if ( is_array($delimiter) ) {
			$string = self::replace($delimiter, self::MULTI_REPLACE_DELIMITER, $string);
			$delimiter = self::MULTI_REPLACE_DELIMITER;
		}

		$return = array();
		$exploded = explode($delimiter, $string);
		foreach ( $exploded as $item ) {
			if ( $item != null && $item != '' ) {
				$return[] = $item;
			}
		}
		return $return;
	}

	/**
	 * Represents a file size with its binary prefix.
	 * @param int $size The file size.
	 * @return string
	 */
	public static function fileSize($size) {
		$size = (int) $size;
		if ( $size >= 1024 ) {
			return round( $size / 1024, 1 ) . ' KiB';
		}
		return $size . ' bytes';
	}

	/**
	 * Returns the length of the given string.
	 * @param string $string The string being measured for length.
	 * @return int The length of the string on success, and 0 if the string is empty.
	 */
	public static function length($str = null) {
		return mb_strlen($str);
	}

	/**
	 * Normalises newlines. Windows (CRLF) and Mac (CR) are converted into Unix (LF) newlines.
	 * @param string $string The string to normalise.
	 * @return string The normalised string.
	 */
	public static function newlines($string = null) {
		$string = self::replace("\r\n", "\n", $string);
		$string = self::replace("\r", "\n", $string);
		return $string;
	}

	/**
	 * Expands all symbolic links and resolves references to '/./', '/../' and extra '/' characters in the input path and returns the canonicalised absolute pathname.
	 * @param string $path The path being checked.
	 * @return string Returns the canonicalized absolute pathname on success. The resulting path will have no symbolic link, '/./' or '/../' components. Returns FALSE on failure, e.g. if the file does not exist.
	 */
	public static function filePath($path = null) {
		$path = realpath2($path);
		return $path;
	}

	/**
	 * Replace all occurrences of the search string with the replacement string.
	 * @param mixed $search The value being searched for, otherwise known as the needle. An array may be used to designate multiple needles.
	 * @param mixed $replace The replacement value that replaces found search values. An array may be used to designate multiple replacements.
	 * @param mixed $string The string or array being searched and replaced on, otherwise known as the haystack. If subject is an array, then the search and replace is performed with every entry of subject, and the return value is an array as well.
	 * @param int $count If passed, this will be set to the number of replacements performed.
	 * @return mixed This function returns a string or an array with the replaced values. 
	 */
	public static function replace($search, $replace, $string = null, $count = null) {
		return str_replace($search, $replace, $string, $count);
	}

	/**
	 * Sanitises a string, making it suitable for permanent links.
	 * @param string $string The string to sanitise.
	 * @return string The sanitised string.
	 */
	public static function sanitise($string = null) {
		$string = self::newlines($string);
		$string = self::replace("\n", null, $string);
		$string = self::lowercase($string);

		$string = self::replace(self::explode(',', self::HTML_ENTITY_SUFFIXES), self::explode(',', self::HTML_ENTITY_SUFFIXES_SAFE), $string);
		$string = htmlentities($string);
		$string = self::replace(self::explode(',', self::HTML_ENTITY_SUFFIXES), null, $string);

		$string = self::replace('amp;', 'and', $string);
		$string = self::replace('sup2;', '2', $string);
		$string = self::replace('sup3;', '3', $string);

		$string = self::replace(str_split(self::SANITISATION_CHARACTERS), null, $string);

		$string = self::replace(' ', '-', $string);
		$string = self::replace('---', '-', $string);
		$string = self::replace('--', '-', $string);

		$string = ltrim(rtrim($string, '-'), '-');

		return $string;
	}

	/**
	 * De-sanitises a sanitised string using best-guess conversion.
	 * @param string $string The string to de-sanitise.
	 * @return string The de-sanitised string.
	 */
	public static function desanitise($string = null) {
		$string = FluentString::replace('-', ' ', $string);
		$string = ucwords($string);
		$_a = array(
			' By ' => ' by ',
			' In ' => ' in ',
			' Of ' => ' of ',
		);
		foreach ( $_a as $_k => $_v ) {
			$string = FluentString::replace($_k, $_v, $string);
		}
		return $string;
	}

	static $numbers = array('zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine');
	public static function say_number($number = 0) {
		if ( count(self::$numbers) > $number ) {
			return self::$numbers[$number];
		}
		return $number;
	}

	public static function starts_with($search, $string = null) {
		return substr($string, 0, self::length($search)) == $search;
	}

	public static function lowercase($string = null, $encoding = null) {
		return mb_convert_case($string, MB_CASE_LOWER, $encoding ? $encoding : mb_detect_encoding($string));
	}
	public static function uppercase($string = null, $encoding = null) {
		return mb_convert_case($string, MB_CASE_UPPER, $encoding ? $encoding : mb_detect_encoding($string));
	}
}
