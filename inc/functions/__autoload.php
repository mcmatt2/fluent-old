<?php
$autoload_include_paths = array(
	'%s/interfaces/%s.php',
	'%s/traits/%s.php',
	'%s/models/%s.php', 
	'%s/controllers/%s.php',
	'%s/%s.php',
);

spl_autoload_register(function($class) use ( $autoload_include_paths, $_site ) {
	if ( class_exists($class) ) {
		return TRUE;
	}

	foreach ( array(SITE_INCLUDES, FLUENT_INCLUDES) as $site ) {
		foreach ( $autoload_include_paths as $path ) {
			$path = realpath2(sprintf($path, $site, $class));

			if ( $path && file_exists($path) ) {
				require($path);
				return TRUE;
			}
		}
	}

	return FALSE;
});
