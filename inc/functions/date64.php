<?php
$date64_shortcuts = array(
	'Month Day, Year' => 'F j, Y',
	'Mon Day' => 'M j',
);

/**
 * Formats a date; returns a string formatted according to the given $format string using the given $time (or the current time if no time is given.)
 * The difference between this function and the native php date() function is that date64() allows years greater than 2038 (Unix time limitation.)
 * @param string $format The format of the outputted date string. See the reference documents at http://php.net/date for more information.
 * @param mixed $time A timestamp to use. The expected format is YYYYMMDDHHMMSS (for example, 20100423013247) or a Unix timestamp.
 * @return mixed
 */
function date64($format = 'YmdHis', $time = null) {
	global $date64_shortcuts;
	if ( array_key_exists($format, $date64_shortcuts) ) {
		$format = $date64_shortcuts[$format];
	}
	try {
		$date = new DateTime($time, new DateTimeZone(date_default_timezone_get()));
		return $date->format($format);
	} catch ( Exception $e ) {
		return $time;
	}
}

define('NOW', date64());
define('NOW_DATE', date64('Ymd', NOW));
define('NOW_TIME', date64('His', NOW));
