<?php
/**
 * Performs the native PHP realpath() function on the $path, then converts backslashes (Windows) into forward slashes (everything else.)
 * @param string $path The path to resolve.
 * @return string
 */
function realpath2($path) {
	return str_replace('\\', '/', realpath($path));
}
