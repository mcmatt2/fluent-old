<?php
/**
 * Recursively converts an array into an object.
 * @param array $a The array to convert.
 * @return object
 */
function fluent_array_to_object($a) {
	foreach ( $a as $_k => $_v ) {
		if ( is_array($_v) ) {
			$a[$_k] = fluent_array_to_object($_v);
		}
	}
	return (object) $a;
}
