<?php
/**
 * Recursively merges an array into another array.
 * @param array $a1 The first array to merge.
 * @param array $a2 The second array to merge.
 * @return array The merged array.
 */
function fluent_array_merge($a1, $a2) {
	foreach ( $a2 as $_k => $_v ) {
		if ( array_key_exists($_k, $a1) && is_array($_v) ) {
			$a1[$_k] = fluent_array_merge($a1[$_k], $a2[$_k]);
		} else $a1[$_k] = $_v;
	}
	return $a1;
}
