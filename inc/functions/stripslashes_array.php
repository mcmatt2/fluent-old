<?php
/**
 * Recursively calls stripslashes() on each member of an array.
 * @param mixed $data The array to call the function on.
 * @return mixed
 */
function stripslashes_array($data) {
	if ( is_array($data) ) {
		foreach ($data as $key => $value ) {
			$data[$key] = stripslashes_array($value);
		}
		return $data;
	} else {
		return stripslashes($data);
	}
}

// Magic quotes are an awful, awful feature. This turns them off.
if ( function_exists('set_magic_quotes_runtime') ) {
	@set_magic_quotes_runtime(FALSE);
}

// Undo the damage caused by magic quotes using the function defined above.
if ( function_exists('get_magic_quotes_gpc') && get_magic_quotes_gpc() ) {
	$_SERVER = stripslashes_array($_SERVER);
	$_GET = stripslashes_array($_GET);
	$_POST = stripslashes_array($_POST);
	$_COOKIE = stripslashes_array($_COOKIE);
	$_FILES = stripslashes_array($_FILES);
	$_ENV = stripslashes_array($_ENV);
	$_REQUEST = stripslashes_array($_REQUEST);
	$HTTP_SERVER_VARS = stripslashes_array($HTTP_SERVER_VARS);
	$HTTP_GET_VARS = stripslashes_array($HTTP_GET_VARS);
	$HTTP_POST_VARS = stripslashes_array($HTTP_POST_VARS);
	$HTTP_COOKIE_VARS = stripslashes_array($HTTP_COOKIE_VARS);
	$HTTP_POST_FILES = stripslashes_array($HTTP_POST_FILES);
	$HTTP_ENV_VARS = stripslashes_array($HTTP_ENV_VARS);
	if ( isset($_SESSION) ) {
		$_SESSION = stripslashes_array($_SESSION);
		$HTTP_SESSION_VARS = stripslashes_array($HTTP_SESSION_VARS);
	}
}
