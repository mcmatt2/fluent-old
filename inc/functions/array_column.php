<?php
if ( !function_exists('array_column') ) {
	/**
	 * Returns the values from a single column of the $input array, identified by the $key.
	 * @see https://wiki.php.net/rfc/array_column
	 * @param array $input A multi-dimensional array (record set) from which to pull a column of values.
	 * @param mixed $key The column of values to return. This value may be the 0-indexed number of the column you wish to retrieve, or it may be the string key name for an associative array.
	 * @return array Returns an array of values representing a single column from the input array.
	 */
	function array_column(array $input, mixed $key) {
		$data = array();
		foreach ( $input as $_i ) {
			$data[] = $_i[$key];
		}
		return $data;
	}
}
