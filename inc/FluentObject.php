<?php
class FluentObject {
	protected
		$_fluent,
		$_site,
		$_db,
		$_tpl,
		$_user;

	public function __construct() {
		global $_fluent, $_site, $_db, $_tpl, $_user;
		$this->_fluent =& $_fluent;
		$this->_site =& $_site;
		$this->_db =& $_db;
		$this->_tpl =& $_tpl;
		$this->_user =& $_user;
	}

	public function __get($var) {
		return NULL;
	}

/*
	public function __construct() { }
	public function __get($var) {
		if ( $var == '_fluent' ) { global $_fluent; return $_fluent; }
		if ( $var == '_site' ) { global $_site; return @$_site; }
		if ( $var == '_db' ) { global $_db; return $_db; }
		if ( $var == '_tpl' ) { global $_tpl; return $_tpl; }
		if ( $var == '_user' ) { global $_user; return $_user; }
		return NULL;
	}
*/
}
