<hr />

<div class="stretch {if $fluent_facebook}with-facebook{/if}">
	<div class="float-left half">
{if $fluent_facebook}
		<h3>Login with Facebook</h3>
		<p>If you have a Facebook account, you can use it to login with one click.</p>
		<p><a class="button facebook" href="login-with-facebook"><span class="sprite facebook"></span> Login with Facebook</a></p>
		<ul>
			<li><a href="privacy-statement">Read our privacy statement</a></li>
		</ul>
{else}
		<h3>Register</h3>
		<p>Sorry; new user registration is currently disabled.</p>
		<p>If you already have a <i>{$SiteName}</i> account, you can login with it.</p>
{/if}
	</div>
	<div class="float-right half">
		<h3>{if $fluent_facebook}Use a {$SiteName} account{else}Login{/if}</h3>
		<form id="login" action="act/login/login-submit" method="post">
			<p>
				<label data-ignore-parse="true" for="login-username">Username:</label><br/>
				<input class="half textbox" id="login-username" name="username" type="text"/>
			</p>

			<p>
				<label data-ignore-parse="true" for="login-password">Password:</label><br/>
				<input class="half textbox" id="login-password" name="password" type="password"/>
			</p>

			<p>
				<input class="button" type="submit" value="Login"/>
			</p>
		</form>
{if true or $fluent_facebook}
	</div>
{/if}
</div>
