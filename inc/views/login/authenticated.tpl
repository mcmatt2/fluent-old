<p>You're currently logged in to {$SiteName} as <a href="~{$_user->username}">{$_user->name}</a>.</p>
<p>If you would like to login to another account, please <a href="logout">log out</a> of this one first.</p>
