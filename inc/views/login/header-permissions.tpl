<div class="messagebox warning">
	<p>{"exclamation"|sprite} <b>Whoa!</b> You tried performing an action for which you do not have permission.</p>
</div>
<p>If you have a user account with the correct permissions, you can login to it here.</p>
