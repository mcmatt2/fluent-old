<h2>Page Not Found</h2>
<p>Sorry, but the page you wanted to load was not found on {$SiteName}.</p>
<p>Here's a few suggestions as to what happened...</p>
<ul>
	<li>Did you type the web address yourself? Check for any misspellings.</li>
	<li>Did you follow a link from another website? Please notify the owner of that site that they have a broken link.</li>
	<li>Did you follow a link from {$SiteName} itself? If so, we may have broken something. Oops!</li>
	<li>The page might not exist. Click the links on the nav bar up there to navigate the website.</li>
</ul>
