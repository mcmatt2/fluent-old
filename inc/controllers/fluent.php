<?php
/**
 * The base class for all Fluent controllers. Provides access to several methods and variables that are useful to all controllers.
 * @property-read FluentDB $_db The connection to the website database.
 * @property-read bool $_fluent Determines if this controller is part of Fluent or a local controller.
 * @property-read FluentTemplate $_tpl The current instance of the template engine.
 * @property-read user $_user The currently logged-in user.
 */
class fluent extends FluentObject {
	protected $_data = array();

	public $name = 'Fluent Core';
	public $codename, $permalink;
	public $description = 'The class which all other controllers are based off.';
	public $require_permissions = array();

	protected $_ajax = false;

	private static $controllers = array();
	public static function & get_controller($codename) {
		$codename = str_replace('-', '_', $codename);
		if ( array_key_exists($codename, self::$controllers) ) {
			return self::$controllers[$codename];
		}

		self::$controllers[$codename] = new $codename();
		return self::$controllers[$codename];
	}

	public function __construct($name = '', $description = '') {
		parent::__construct();
		$this->codename = get_called_class();
		$this->name = $name ?: $this->name;
		$this->description = $description ?: $this->description;
		$this->permalink = $this->codename_to_permalink($this->codename);

		$this->_ajax = ( $this->_server('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest' ) || ( (boolean) $this->_get('_ajax', 0) );
	}

	public function __get($var) {
		if ( in_array($var, (array) $this->_site->fluent) ) {
			return $this->_site->fluent->$var;
		}
		if ( $var == '_cache' ) {
			return c('cache');
		}
		if ( $var == 'fluent' ) {
			$codename = $this->permalink_to_codename($this->codename);
			return file_exists(FLUENT_CONTROLLERS . "/{$codename}.php");
		}
		if ( array_key_exists($var, $this->_data) ) {
			return $this->_data[$var];
		}
		return parent::__get($var);
	}

	public function __set($var, $val) {
		$this->_data[$var] = $val;
	}

	public function __call($method, $arguments) {
		if ( !$this->method_exists($method) ) {
			throw new Exception("The '" . $this->codename ."' module does not contain a method named '" . $method . "', and no suitable overrides were found.");
		}
	}

	public function shouldReturn404() {
		return !$this->method_exists('content');
	}

	public function shouldReturn403() {
		return false;
	}

	public function getPageTitle() {
		return $this->name;
	}

	public function getPageMetaDescription() {
		return $this->_site->metadata->description ?: null;
	}

	public function getPageMetaKeywords() {
		return $this->_site->metadata->keywords ?: null;
	}

	public function getPageMetaLanguage() {
		return $this->_site->metadata->language ?: null;
	}

	/**
	 * Provides an easy way to access $_REQUEST variables.
	 * @param string $var The variable to access.
	 * @param string $else If the variable is not found, then return this value (defaults to null)
	 * @return string The value of the requested variable; $else otherwise.
	 */
	public function request(&$var, &$else = null, &$src = null) {
		if ( $src == null ) {
			$src = $_REQUEST;
		}
		return (array_key_exists($var, $src) && $src[$var] != '' ? $src[$var] : $else);
	}
	public function _get($var, $else = null) { return $this->request($var, $else, $_GET); }
	public function _post($var, $else = null) { return $this->request($var, $else, $_POST); }
	public function _cookie($var, $else = null) { return $this->request($var, $else, $_COOKIE); }
	public function _session($var, $else = null) { return $this->request($var, $else, $_SESSION); }
	public function _server($var, $else = null) { return $this->request($var, $else, $_SERVER); }

	/**
	 * Redirect the user to a specified page.
	 */
	protected function _redirect_to($url, $message = null) {
		if ( $message != '' ) {
			$_SESSION['fluent_message'] = $message;
		}
		header('Location: ' . URL . $url);
		die();
	}

	protected function _error($message, $response_code = null) {
		if ( $message instanceof Exception ) {
			$message = $message->getMessage();
		}
		if ( $response_code ) {
			$this->_response($response_code);
		}
		if ( $this->_ajax ) {
			$response = @json_encode((object) array('error' => $message));
			die($response);
		} else {
			die('Error: ' . $message);
		}
	}

	private $_response_codes = array(
		200 => 'OK',
		403 => 'Forbidden',
		404 => 'Not Found',
		500 => 'Internal Server Error',
	);
	public function _response($code = 200, $content_type = null, $message = null) {
		header($_SERVER['SERVER_PROTOCOL'] . ' ' . $code . ' ' . $this->_response_codes[$code]);
		if ( $content_type ) {
			header('Content-Type: ' . $content_type . '; charset=UTF-8', true);
		}
		if ( $message ) {
			if ( $this->_ajax ) {
				try {
					return json_encode(is_array($message) ? $message : array('message' => $message));
				} catch(Exception $e) {
					return 'An error occurred.';
				}
			} else if ( $code == 403 ) return $this->_response_codes[$code];
		}
	}

	public function _updateCookie($name, $value = null) {
		$host = FluentString::explode('/', URL);
        $host = $host[0];
		if ( $value == null || strlen($value) <= 0 ) {
			setcookie($name, '', time() - 3600, '/', $host, false, true);
		} else {
			setcookie($name, $value, strtotime('+1 month'), '/', $host, false, true);
		}
	}
	public function _deleteCookie($name) {
		return $this->_updateCookie($name);
	}



	/* controller loading and unloading */

	/**
	 * Checks if a controller is enabled. This checks both Fluent and local controllers (local first).
	 * @param string $codename The codename of the controller to check.
	 * @return bool true, if the controller exists.
	 */
	public function controller_enabled($codename) {
		$codename = $this->permalink_to_codename($codename);
		return file_exists(SITE_CONTROLLERS . '/' . $codename . '.php') || file_exists(FLUENT_CONTROLLERS . '/' . $codename . '.php');
	}

	/**
	 * Displays a Smarty template from the current controller directory. Also applies a variable '$controller' to Smarty, containing a reference to '$this'.
	 * @param string $tpl The name of the template to display.
	 * @param array $vars An optional array containing $variable => $value pairs to be assiged to the template engine.
	 */
	public function display($tpl, $vars = null) {
		$tpl = ( $this->fluent ? '\\' : null ) . $this->codename . '/' . $tpl;
		$codename = $this->codename;
		$this->_tpl->assign('controller', $this);
		$this->_tpl->display($tpl, $vars);
	}
	public function assign($var, $val = null) {
		if ( is_array($var) ) {
			foreach ( $var as $k => $v ) {
				$this->assign($k, $v);
			}
			return;
		}
		return $this->_tpl->assign($var, $val);
	}

	public function has_template($tpl) {
		$codename = $this->codename;
		return file_exists(($this->fluent ? FLUENT_CONTROLLERS : SITE_CONTROLLERS) . '/templates/' . $codename . '/' . $tpl . '.tpl');
	}

	public function has_permissions($permissions) {
		$permissions = fluent::to_array($permissions);
		foreach ( $permissions as $permission ) {
			if ( !$this->has_permission($permission) ) {
				return false;
			}
		}
		return true;
	}
	public function has_permission($permission) {
		$permission = fluent::cast('permission', $permission);
		if ( !$this->_user->has_permission($permission) ) {
			$this->_tpl->assign('module', $this);
			$this->_tpl->assign('permission', $permission);
			$this->_tpl->display(FLUENT_CONTROLLERS . '/templates/fluent/no_permission.tpl');
			return false;
		}
		return true;
	}

	public static function cast($type, &$object = null) {
		if ( $object == null || is_string($object) || is_int($object) || is_numeric($object) ) {
			return call_user_func($type, $object);
		}
		return $object;
	}

	/**
	 * Checks if the passed object is an array. If not, an array will be created containg the object as its first element.
	 * Either way, an array containing objects is returned.
	 * @static
	 * @param mixed $object The object to check.
	 * @return array The array of objects.
	 */
	public static function to_array($object) {
		if ( !is_array($object) ) $object = array($object);
		return $object;
	}

	/**
	 * Checks if this controller contains a method. Mostly used to determine whether or not to show content/sidebar blocks.
	 * @param string $method The name of the method.
	 * @return bool true, if the method exists.
	 */
	public function method_exists($method) {
		$method = $this->permalink_to_codename($method);
		return method_exists($this->codename, $method);
	}

	/**
	 * Converts a codename to a permalink (underscores to hyphens).
	 * @param $codename string the codename to convert.
	 * @return string the converted permalink.
	 */
	protected function codename_to_permalink($codename) {
		return FluentString::replace('_', '-', $codename);
	}

	/**
	 * Converts a permalink to a codename (hyphens to underscores).
	 * @param $permalink string the permalink to convert.
	 * @return string the converted codename.
	 */
	protected function permalink_to_codename($permalink) {
		return FluentString::replace('-', '_', $permalink);
	}



	public function has_content() {
		return $this->method_exists('content');
	}
	public function has_sidebar() {
		return $this->method_exists('sidebar');
	}
}

function controller($codename) {
	return fluent::get_controller($codename);
}

function c($c) {
	return controller($c);
}

/**
 * Checks if a controller is enabled. Global call to $_fluent->controller_enabled($codename).
 * @param string $codename The controller to check.
 * @return bool true, if the controller is enabled.
 */
function c_enabled($codename) {
	global $_fluent;
	return $_fluent->controller_enabled($codename);
}

/**
 * Checks if a controller is disabled. Global call to $fluent->controller_enabled($codename); result inverted.
 * @param string $codename The controller to check.
 * @return bool true, if the controller is disabled.
 */
function c_disabled($codename) {
	return !c_enabled($codename);
}

function fluent_call($controller, $method, $parameters = NULL) {
	return controller($controller)->$method($parameters);
}
