<?php
class auth_facebook extends fluent {
	public function __construct() {
		parent::__construct('Fluent: Facebook Authentication', 'Allows users to login via Facebook, and links Facebook accounts to local accounts.');
	}

	public function enabled() {
		return @FluentString::length($this->_site->auth->facebook->app) > 0 && FluentString::length($this->_site->auth->facebook->secret) > 0;
	}

	function get_login_url() {
		return 'https://www.facebook.com/dialog/oauth?client_id=' . $this->_site->auth->facebook->app . '&redirect_uri=' . urlencode(URL . '/');
	}

	function redirect_to_login_url() {
		header('Location: ' . $this->get_login_url(), true, 302);
		die();
	}

	function download($url) {
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch, CURLOPT_CAINFO, DIR_DATA . '/ssl.crt');
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}

	function oauth_authenticate() {
		if ( $this->request('code') == '' || $this->request('error_reason') == '' || $this->request('error_description') == '' ) {
			die('That action is not available at this time.');
		}
		if ( $this->request('code') == '' ) {
			die("Facebook returned an error. That error was: '{$_REQUEST['error_description']}'. Please try again.");
		}
		$code = $this->request('code');
		$url = 'https://graph.facebook.com/oauth/access_token?client_id=' . $this->_site->auth->facebook->app . '&redirect_uri=' . urlencode(URL . '/') . '&client_secret=' . $this->_site->auth->facebook->secret . '&code=' . $code;
		$data = $this->download($url);
		if ( $data == '' ) {
			die('There was a problem connecting to Facebook. Please try again.');
		}
		if ( strpos($data, 'access_token=') === false ) {
			die('Facebook returned an error. Please try again.');
		}
		$data = explode('&expires=', $data);
		$data = explode('access_token=', $data[0]);
		$token = $data[1];
		$user = json_decode($this->download('https://graph.facebook.com/me?access_token=' . $token));
		if ( !$user || !$user->id || !($user->id > 0) ) {
			die('Facebook returned an invalid user ID.');
		}
		$this->oauth_authenticate_local($user);
	}

	function oauth_authenticate_local($user) {
		if ( !$user || !$user->id || !$user->id > 0 ) {
			return;
		}

		$remote_id = "fb-{$user->id}";
		$local_id = user::get_id_from_thirdparty($remote_id);

		if ( $local_id ) { // This Facebook user has been here before.
			$account = user::get_credentials_from_id($local_id);
//			$this->login->account_login(array('id' => $local_id));
			// todo: move this to a proper file
			setcookie('fluent_credentials', '', time() - 3600, '/', '.' . $_SERVER['HTTP_HOST'], false, true);
			setcookie('fluent_credentials', json_encode((object) array('user' => $account->username, 'pass_hash' => $account->password)), strtotime('+1 month'), '/', '.' . $_SERVER['HTTP_HOST'], false, true);
			//$this->my->set('nickname', $user->name, $local_id);
			user($local_id)->nickname = $user->name;
			header('Location: ' . URL . '/login');
			die();
			//$this->redirect_to('/comics', 'Welcome back, ' . $user->name . '! You\'re being redirected to your favourite comics now.');
		} else { // This Facebook user is new to the site.
/*
			$username = $remote_id;
			$password = 'my-' . date64('Ymd');
			if ( $local_id = $this->login->account_create(array('username' => $username, 'password' => $password)) ) {
				$this->login->account_connect(array('id' => $remote_id, 'user' => $local_id));
				$this->login->account_login(array('id' => $local_id));
				$this->my->set('nickname', $user->name, $local_id);
				$this->redirect_to('/comics&registered=true', 'Welcome to ' . setting('meta/site-name')->value . ', ' . $user->name . '! You\'re being redirected to your new profile now.');
			}
*/
		}
	}
}
