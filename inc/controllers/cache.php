<?php
class cache extends fluent {
	var $_cache;

	public function __construct() {
		parent::__construct('Cache', 'Caches data, allowing users to access it quickly.');
		$this->_cache = $this->_site->directory . '/cache';
	}

	/**
	 * Determines if the cache contains an element with a certain key.
	 * @param string $key The cache key.
	 * @return boolean
	 */
	public function exists($key) {
		$key = $this->sanitise_key($key);
		return is_readable($this->_cache) && file_exists($this->_cache . '/' . $key);
	}

	/**
	 * Gets data from the cache. Returns false if no cached data exists.
	 * @param string $key The cache key.
	 * @return mixed
	 */
	public function get($key) {
		$key = $this->sanitise_key($key);
		if ( $this->exists($key) ) {
			return file_get_contents($this->_cache . '/' . $key);
		} else return false;
	}

	/**
	 * Stores data in the cache. Returns true if the cache was successful (false otherwise).
	 * @param string $key The cache key.
	 * @param string $data The data to cache.
	 * @return boolean
	 */
	public function put($key, $data) {
		$key = $this->sanitise_key($key);
		if ( is_writable($this->_cache) ) {
			file_put_contents($this->_cache . '/' . $key, $data);
			return true;
		}// else throw new Exception('An object with the key "' . $key . '" could not be cached to ' . $this->_cache . '/' . $key . ', because the destination was unwritable.');
		return false;
	}

	/**
	 * Sanitises a key, rendering it suitable for use as a filename.
	 * @param string $key The key to sanitise.
	 * @return string The sanitised key.
	 */
	private function sanitise_key($key) {
		$pph = 'periodplaceholder';
		$key = FluentString::replace('.', $pph, $key);
		$key = FluentString::sanitise($key);
		$key = FluentString::replace($pph, '.', $key);
		return $key;
	}
}
