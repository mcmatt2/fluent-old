<?php
class error404 extends fluent {
	public function __construct() {
		parent::__construct('Fluent: Error 404', 'Provides information to users who request pages that do not exist.');
	}

	public function getPageTitle() {
		return 'Page Not Found';
	}

	public function content() {
		$this->display('content');
	}
}
