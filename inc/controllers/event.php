<?php
class event extends fluent {
	private $events;

	public function __construct() {
		$this->events = array();
		parent::__construct('Fluent: Event Handler', 'Allows controllers to fire and react to events.');
	}

	public function listen($event, $func) {
		if ( !array_key_exists($event, $this->events) ) {
			$this->events[$event] = array();
		}
		$this->events[$event][] = $func;
	}

	public function fire($event, $args) {
		if ( is_array($args) ) {
			$args = (object) $args;
		}
		if ( array_key_exists($event, $this->events) ) {
			for ( $i = 0; $i < count($this->events[$event]); $i++ ) {
				$e = $this->events[$event][$i];
				if ( !is_array($e) && is_callable($e) ) {
					$e($args);
				} else {
					$e = explode('/', $e);
					c($e[0])->$e[1]($args);
				}
			}
		}
	}
}
