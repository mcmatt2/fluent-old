<?php
class login extends fluent {
	public function __construct() {
		parent::__construct('Fluent: Login Handler', 'Provides access to the user accounts in this website\'s database.');
	}

	public function getPageTitle() {
		return 'Login';
	}

	public function content() {
		$this->display('header');
		$_m = $this->_get('controller');
		if ( $_m !== $this->permalink ) {
			$this->display('header-permissions');
			$this->display('content');
			return;
		}
		if ( $this->_user && $this->_user->id ) {
			$this->display('authenticated');
			return;
		}
		$this->display('header-login');
		$this->display('content');
	}

	public function do_logout() {
		$this->_deleteCookie('fluent_credentials', null);
		header('Location: ' . URL . '/login');
	}

	public function login_submit() {
		$user = $this->_post('username', '');
		$pass = $this->_post('password', '');
		if ( $user != '' && $pass != '' ) {
			$pass_hash = sha1($pass);
			if ( user::account_exists($user, $pass_hash) ) {
				$this->_updateCookie('fluent_credentials', json_encode((object) array('user' => $user, 'pass_hash' => $pass_hash)));
				$_user = user(user::get_id_from_credentials($user, $pass_hash));

				if ( $this->_db->table_has_field('\\users', 'last_login') ) {
					$_user->last_login = NOW;
					$_user->commit();
				}

				header('Location: ' . URL . '/login');
			} else die('Your username or password was incorrect.');
		} else die('Please enter your account details.');
	}

	public function get_user() {
		$_user = null;

		if ( $auth = $this->_cookie('fluent_credentials') ) {
			$auth = json_decode($auth, true);
			if ( !$auth['user'] || !$auth['pass_hash'] ) {
				return FALSE;
			}
			$user = $auth['user'];
			$pass = $auth['pass_hash'];

			if ( !user::account_exists($user, $pass) ) {
				return FALSE;
			}

			$id = user::get_id_from_credentials($user, $pass);
			$_user = user($id);
		}

		if ( !$_user ) {
			$_user = user(0);
		}

		if ( $_user->id && $this->_db->table_has_field('\\users', 'last_active') ) {
			$_user->last_active = NOW;
			$_user->commit();
		}

		return $_user;
	}
}
