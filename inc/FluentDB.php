<?php
class FluentDB extends PDO {
	var $_prefix, $_prefix_fluent;
	private $_tables = array();

	public function __construct($dsn, $username = null, $password = null, $driver_options = array()) {
		global $_site;
		$this->_prefix = $_site->database->prefix;
		$this->_prefix_fluent = $_site->fluent->database->prefix;

		parent::__construct($dsn, $username, $password, $driver_options);
		$this->query('SET NAMES utf8;');
		$this->query('SET CHARACTER SET utf8;');

		$tables = $this->query('SHOW TABLES;');
		foreach ( $tables as $_t ) {
			$fields = $this->query('DESCRIBE ' . $_t[0] . ';');
			$_t = str_replace(array($this->_prefix, $this->_prefix_fluent), array(null, '\\'), $_t[0]);
			$this->_tables[$_t] = array();
			foreach ( $fields as $_f ) {
				$this->_tables[$_t][] = $_f['Field'];
			}
		}
	}

	public function prepare($sql, $options = array()) {
		if ( !is_array($options) ) {
			$options = array($options);
		}
		$sql = FluentString::replace('___\\', $this->_prefix_fluent, $sql);
		$sql = FluentString::replace('___', $this->_prefix, $sql);
		return parent::prepare($sql, $options);
	}

	public function query($sql, $params = null, $fetchAll = true) {
		if ( !is_array($params) ) {
			$params = array($params);
		}
		$statement = $this->prepare($sql);
		$statement->execute($params);
		return $fetchAll ? $statement->fetchAll() : null;
	}

	public function unsilence() {
		$this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	public function silence() {
		$this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
	}

	/**
	 * Start a transaction, shadowing all queries made from this point forward.
	 */
	public function store_changes() {
		return $this->query('START TRANSACTION;');
	}

	/**
	 * Rollback an in-progress transaction, undoing all changes made since the transaction was started.
	 */
	public function cancel_changes() {
		return $this->query('ROLLBACK;');
	}

	/**
	 * Commit an in-progress transaction, saving all changes to the database and ending the transaction.
	 */
	public function commit_changes() {
		return $this->query('COMMIT;');
	}

	/**
	 * Determines if a given table exists in the database.
	 * @param string $table The table to check (without the prefix.)
	 * @return boolean
	 */
	public function has_table($table) {
		return array_key_exists($table, $this->_tables);
	}

	/**
	 * Determines if a given field exists in a table.
	 * @param string $table The table to check (without the prefix.)
	 * @param string $field The field to check.
	 * @return boolean
	 */
	public function table_has_field($table, $field) {
		return array_key_exists($table, $this->_tables) && in_array($field, $this->_tables[$table]);
	}

	public function get_table_fields($table) {
		return is_array($this->_tables) && array_key_exists($table, $this->_tables) ? $this->_tables[$table] : null;
	}

	/**
	 * Sanitises a table name, removing its global prefix in the process.
	 * @param string $table The table name to sanitise.
	 * @return string
	 */
	private function sanitise_table_name($table) {
		if ( FluentString::starts_with($this->_prefix, $table) ) {
			$table = substr($table, FluentString::length($this->_prefix));
		} else if ( FluentString::starts_with($this->_prefix_fluent, $table) ) {
			$table = '\\' . substr($table, FluentString::length($this->_prefix_fluent));
		} else if ( FluentString::starts_with('___', $table) ) {
			$table = substr($table, 3);
		}
		return $table;
	}

	/**
	 * Given two objects, this method will return a string containing the name of the database table which contains obj1:obj2 pairs. If a suitable table doesn't exist, the method will return null.
	 * @param FluentModel $obj1 The first object to compare.
	 * @param FluentModel $obj2 The second object to compare.
	 * @return mixed
	 */
	public function get_linked_table_name($obj1 = null, $obj2 = null) {
		if ( @!$obj1 || @!$obj2 ) {
			return null;
		}

		$fluent = ( $obj1->_isFluent || $obj2->_isFluent );
		$table = '';
		$table1 = ( $fluent ? '\\' : null ) . FluentString::replace('\\', null, '_' . $obj1->_table . '_' . $obj2->_table );
		$table2 = ( $fluent ? '\\' : null ) . FluentString::replace('\\', null, '_' . $obj2->_table . '_' . $obj1->_table );

		if ( $this->has_table($table1) ) {
			$table = $table1;
		} else if ( $this->has_table($table2) ) {
			$table = $table2;
		}
		if ( !$table || $table == '' ) {
			return null;
		}

		return '___' . $table;
	}

	/**
	 * Determines if two objects have a link (checks a [prefix_]obj1_obj2 table.) The order in which the objects are passed bears no relevance to the outcome of this method.
	 * @param FluentModel $obj1 The first object to compare.
	 * @param FluentModel $obj2 The second object to compare.
	 * @return boolean
	 */
	public function has_link($obj1 = null, $obj2 = null) {
		// If either of the two objects are invalid, there's no point continuing.
		if ( !$obj1 || !$obj1->id || !$obj2 || !$obj2->id ) {
			return false;
		}

		$table = $this->get_linked_table_name($obj1, $obj2);

		if ( !$table ) {
			return false;
		}


		// if obj1 and obj2 from table return true
		$cmp = $this->query("SELECT COUNT(*) FROM `{$table}` WHERE `{$obj1->_class}` = ? AND `{$obj2->_class}` = ?", array($obj1->id, $obj2->id));
		return @($cmp[0][0] > 0);
	}

	public function get_links_of_type($obj = null, $type = null) {
		$o1 = $obj;
		$o2 = $type::get(NULL);
		$table = $this->get_linked_table_name($o1, $o2);
		$_links = array();
		if ( !$table ) {
			return $_links;
		}
		foreach ( $this->query("SELECT * FROM `{$table}` WHERE `{$o1->_class}` = ?", array($o1->id)) as $_r ) {
			$_links[] = call_user_func($type, $_r[$o2->_class]);
		}
		return $_links;
	}

	/**
	 * Creates a link between two objects. Returns true if the link was created successfully; false otherwise.
	 * @param FluentModel $obj1 The first object.
	 * @param FluentModel $obj2 The second object.
	 * @return boolean
	 */
	public function create_link($obj1 = null, $obj2 = null) {
		if ( @!$obj1 || @!$obj2 || @!$obj1->id || @!$obj2->id ) {
			return false;
		}

		$table = $this->get_linked_table_name($obj1, $obj2);

		if ( !$table ) {
			return false;
		}

		if ( $this->has_link($obj1, $obj2) ) {
			return true;
		}

		$this->query(
			"INSERT INTO `{$table}` (`{$obj1->_class}`, `{$obj2->_class}`) VALUES(?, ?)",
			array(
				$obj1->id,
				$obj2->id,
			)
		);

		return $this->has_link($obj1, $obj2);
	}

	/**
	 * Deletes a link between two objects. Return true if the link was deleted successfully; false otherwise.
	 * @param FluentModel $obj1 The first object.
	 * @param FluentModel $obj2 The second object.
	 * @return boolean
	 */
	public function delete_link($obj1 = null, $obj2 = null) {
		if ( @!$obj1 || @!$obj2 || @!$obj1->id || @!$obj2->id ) {
			return false;
		}

		$table = $this->get_linked_table_name($obj1, $obj2);
		if ( !$table ) {
			return false;
		}

		if ( !$this->has_link($obj1, $obj2) ) {
			return true;
		}

		$this->query(
			"DELETE FROM `{$table}` WHERE `{$obj1->_class}` = ? AND `{$obj2->_class}` = ? LIMIT 1",
			array(
				$obj1->id,
				$obj2->id,
			)
		);

		return !$this->has_link($obj1, $obj2);
	}
}
