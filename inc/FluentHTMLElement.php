<?php
/**
 * Represents a HTML element.
 * @author Matt McMahon
 */
class FluentHTMLElement {
	var $element;
	var $attributes = array();
	var $classes = array();

	var $elements = array();

	/**
	 * @param string $element The HTML element that this instance will represent (a, img, h1, etc.)
	 */
	public function __construct($element) {
		$this->element = $element;
	}

	private function _array_remove($array, $item) {
		return array_diff($array, array($item));
	}



	/**
	 * Returns the HTML element associated with this instance.
	 * @return string
	 */
	public function get_element() {
		return $this->element;
	}

	/**
	 * Sets the HTML element associated with this instance.
	 * @param string $element The new HTML element that this instance will represent.
	 * @return self
	 */
	public function set_element($element) {
		$this->element = $element;
		return $this;
	}



	/**
	 * Returns the value of a previously-defined attribute.
	 * @param string $attribute The name of the attribute to fetch the value of.
	 * @return string
	 */
	public function get($attribute) {
		if ( array_key_exists($attribute, $this->attributes) ) {
			return $this->attributes[$attribute];
		}
		return null;
	}

	/**
	 * Sets an attribute for this element.
	 * @param string $attribute The name of the attribute to set.
	 * @param string $value     The value to assign to the attribute.
	 * @return self
	 */
	public function set($attribute, $value) {
		$this->attributes[$attribute] = $value;
		ksort($this->attributes);
		return $this;
	}

	/**
	 * jQuery-like 'attr' method to quickly set or get the value of an attribute. If a value is passed, the attribute will be set to that value. Otherwise, the existing value of that attribute will be returned.
	 * @param string $attribute The name of attribute to get or set.
	 * @param string $value     Optional. The value to assign to the attribute.
	 * @return mixed
	 */
	public function attr($attribute = null, $value = null) {
		if ( $attribute && $value ) {
			$this->set($attribute, $value);
			return $this;
		} else {
			return $this->get($attribute);
		}
	}



	/**
	 * Adds a CSS class to this element (if it doesn't already have that class.)
	 * @param string $class The CSS class to add to this element.
	 * @return self
	 */
	public function add_class($class) {
		if ( is_array($class) ) {
			foreach ( $class as $_c ) {
				$this->add_class($_c);
			}
			return $this;
		}
		if ( !in_array($class, $this->classes) ) {
			$this->classes[] = $class;
		}
		return $this;
	}

	/**
	 * Removes a CSS class from this element.
	 * @param string $class The CSS class to remove from this element.
	 * @return self
	 */
	public function remove_class($class) {
		if ( in_array($class, $this->classes) ) {
			$this->_array_remove($this->classes, $class);
		}
		return $this;
	}



	/**
	 * Inserts a child element at the end of this element.
	 * @param mixed $element FluentHTMLElement, or string to insert at the end of this element.
	 * @return self
	 */
	public function append() {
		if ( func_num_args() > 1 ) {
			foreach ( func_get_args() as $_e ) {
				$this->append($_e);
			}
			return $this;
		}
		$element = func_get_arg(0);
		if ( is_string($element) || ( $element instanceof self && !in_array($element, $this->elements) ) ) {
			$this->elements[] = $element;
		}
		return $this;
	}

	/**
	 * Insert this element at the end of another element. This is equivalent to calling $element->append($this);
	 * @param FluentHTMLElement $element The element to append this element to.
	 * @return self
	 */
	public function append_to($element) {
		if ( $element instanceof self ) {
			$element->append($this);
			return $this;
		}
		throw new Exception('A FluentHTMLElement tried attaching itself to an incompatible object.');
	}



	/**
	 * Inserts a child element at the start of this element.
	 * @param mixed $element FluentHTMLElement, or string to insert at the start of this element.
	 * @return self
	 */
	public function prepend($element) {
		if ( is_array($element) ) {
			foreach ( $element as $_e ) {
				$this->prepend($_e);
			}
			return $this;
		}
		if ( is_string($element) || ( $element instanceof self && !in_array($this->elements) ) ) {
			array_unshift($this->elements, $element);
		}
		return $this;
	}

	/**
	 * Insert this element at the start of another element. This is equivalent to calling $element->prepend($this);
	 * @param FluentHTMLElement $element The element to prepend this element to.
	 * @return self
	 */
	public function prepend_to($element) {
		if ( $element instanceof self ) {
			$element->prepend($this);
			return $this;
		}
		throw new Exception('A FluentHTMLElement tried attaching itself to an incompatible object.');
	}



	/**
	 * Removes a child element from this element. The child element can either be an instance of the FluentHTMLElement class, or a plain string.
	 * @param mixed $element The child element to remove.
	 * @return self
	 */
	public function remove_child($element) {
		if ( in_array($element, $this->elements) ) {
			$this->_array_remove($this->elements, $element);
		}
		return $this;
	}

	/**
	 * Removes all children elements from this element.
	 * @return self
	 */
	public function empty_elements() {
		unset($this->elements);
		$this->elements = array();
		return $this;
	}



	private static $self_closing_elements = array('area', 'base', 'br', 'col', 'command', 'embed', 'hr', 'img', 'input', 'keygen', 'link', 'meta', 'param', 'source', 'track', 'wbr');
	public function __toString() {
		$html = '<' . $this->element;
		if ( count($this->classes) ) {
			$html .= ' class="';
			$classes = null;
			foreach ( $this->classes as $class ) {
				$classes .= $class . ' ';
			}
			$classes = trim($classes);
			$html .= $classes . '"';
		}
		if ( count($this->attributes) ) {
			foreach ( $this->attributes as $_k => $_v ) {
				$html .= ' ' . $_k . '="' . $_v . '"';
			}
		}
		$html = trim($html);
		if ( count($this->elements) ) {
			$html .= '>';
			foreach ( $this->elements as $_c ) {
				$html .= $_c;
			}
			$html .= '</' . $this->element . '>';
		} else {
			if ( in_array($this->element, self::$self_closing_elements) ) {
				$html .= ' />';
			} else {
				$html .= '></' . $this->element . '>';
			}
		}
		return $html;
	}

	/**
	 * Returns a new instance of FluentHTMLElement using the specified element.
	 * @param string $element The HTML element that this instance will represent (a, img, h1, etc.)
	 * @return FluentHTMLElement
	 */
	public static function create($element) {
		return new self($element);
	}

	public static function create_hyperlink($text, $url) {
		$el = self::create('a');
		$el->attr('href', $url);
		$el->add_element($text);
		return $el;
	}
}
