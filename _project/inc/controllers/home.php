<?php
class home extends fluent {
	public function __construct() {
		parent::__construct('Home', 'This controller handles the home page.');
	}

	public function content() {
		$this->display('content');
	}
}
