<?php
require_once('config.php');

$_tpl->display('header', [ 'page_title' => $_controller->getPageTitle() . ' &laquo; ' . $_site->name ]);
$_controller->content();
$_tpl->display('footer');
