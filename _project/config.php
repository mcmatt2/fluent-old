<?php
$_site = array(
	'name' => 'My Site',
	'database' => array(
		'host' => NULL,
		'name' => NULL,
		'user' => NULL,
		'pass' => NULL,
		'prefix' => NULL,
	),
	'auth' => array(
		'facebook' => array(
			'app' => NULL,
			'secret' => NULL,
		),
	),
	'metadata' => array(
		'description' => NULL,
		'keywords' => NULL,
	),
	'fluent' => array(
		'disable_caching' => FALSE,
	),
);

require_once('./path_to/libfluent.php');

fluent_initialise();
