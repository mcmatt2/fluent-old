<?php
ob_start("ob_gzhandler");
define('DEBUG', file_exists('../.debug'));

$_fluent = null;
$_db = null;
$_tpl = null;
$_user = null;

require_once('inc/FluentException.php');

error_reporting(DEBUG ? E_ALL : 0);

date_default_timezone_set('Europe/London');
setlocale(LC_ALL, array('en_GB.UTF8', 'en_GB', 'english'));
if ( function_exists('mb_internal_encoding') ) {
	mb_internal_encoding('UTF-8');
}

// Check if this was included, and if the correct constant was set.
if ( !$_site || ( !is_array($_site) && !is_string($_site) ) ) {
	throw new Exception('Fluent can\'t be called directly; it must be embedded and used as part of a larger site.');
}

foreach ( glob(__DIR__ . '/inc/functions/*.php') as $_f ) { require($_f); }

$defaults = array(
	'name' => NULL,
	'directory' => NULL,
	'database' => array(
		'host' => '127.0.0.1',
		'name' => NULL,
		'user' => NULL,
		'pass' => NULL,
		'prefix' => NULL,
	),
	'metadata' => array(
		'description' => NULL,
		'keywords' => NULL,
		'language' => 'en-GB',
	),
	'auth' => array(
		'facebook' => array(
			'app' => NULL,
			'secret' => NULL,
		),
	),
	'fluent' => array(
		'directory' => realpath2(__DIR__),
		'database' => array(
			'prefix' => 'fluent_',
		),
		'disable_caching' => FALSE,
	),
);

$dir = explode('/', realpath2($_SERVER['SCRIPT_FILENAME']));
unset($dir[count($dir) - 1]);
$dir = implode('/', $dir);
$defaults['directory'] = realpath2($dir);
unset($dir);

if ( is_array($_site) ) {
	$_site = fluent_array_merge($defaults, $_site);
} else if ( is_string($_site) ) {
	$file = realpath2($defaults['directory'] . '/' . $_site);
	if ( !file_exists($file) ) {
		throw new Exception('Site configuration file not found.');
		die();
	}
	$_site = fluent_array_merge($defaults, @json_decode(file_get_contents($file), true));
} else {
	throw new Exception('Site configuration was in an unrecognised format.');
}

$_site = fluent_array_to_object($_site);

header('Content-Type: text/html; Charset=UTF-8');

// Configure directories.
define('FLUENT', realpath2(dirname(__FILE__)));
define('FLUENT_INCLUDES', FLUENT . '/inc');
define('FLUENT_THIRDPARTY', FLUENT_INCLUDES . '/thirdparty');
define('FLUENT_INTERFACES', FLUENT_INCLUDES . '/interfaces');
define('FLUENT_MODELS', FLUENT_INCLUDES . '/models');
define('FLUENT_VIEWS', FLUENT_INCLUDES . '/views');
define('FLUENT_CONTROLLERS', FLUENT_INCLUDES . '/controllers');

define('SITE', realpath2($_site->directory));
define('SITE_INCLUDES', SITE . '/inc');
define('SITE_THIRDPARTY', SITE_INCLUDES . '/thirdparty');
define('SITE_INTERFACES', SITE_INCLUDES . '/interfaces');
define('SITE_MODELS', SITE_INCLUDES . '/models');
define('SITE_VIEWS', SITE_INCLUDES . '/views');
define('SITE_CONTROLLERS', SITE_INCLUDES . '/controllers');

define('URL', ( '//' . $_SERVER['SERVER_NAME'] . str_replace('/index.php', null, $_SERVER['PHP_SELF']) ));
define('IP', $_SERVER['REMOTE_ADDR']);



$_db = new FluentDB(sprintf("mysql:host=%s;dbname=%s;charset=utf8", $_site->database->host, $_site->database->name), $_site->database->user, $_site->database->pass);
$_tpl = new FluentTemplate();

require_once(FLUENT_CONTROLLERS . '/fluent.php');
$_fluent = controller('fluent');

// Markdown.
require_once(FLUENT_THIRDPARTY . '/Markdown.php');

// Handles calling objects in the following form: book(1), etc. Local site models take precedence over Fluent models.
foreach ( array(SITE_MODELS, FLUENT_MODELS) as $path ) {
	foreach ( glob("{$path}/*.php") as $file ) {
		$name = FluentString::replace(array($path . '/', '.php'), null, $file);
		if ( !function_exists($name) && !in_array($name, array('FluentModel', 'iFluentModel')) ) {
			new $name(null);
			eval("function & $name(\$id = null) { return $name::get(\$id); }");
		}
	}
}

// Quick way of checking if Facebook login is enabled.
define('FLUENT_FACEBOOK', FALSE);
$_tpl->assign('fluent_facebook', FLUENT_FACEBOOK);

// Let's log in the user if their details check out.
$_user = controller('login')->get_user();

$_fluent = controller('fluent');
$_controller = null;

// Might as well make a few variables accessible to the template engine.
$_tpl->assign('URL', URL);
$_tpl->assign('SiteName', $_site->name);

$_tpl->assign('_fluent', $_fluent);
$_tpl->assign('_controller', controller('error404'));
$_tpl->assign('_user', $_user);

// We heard you like template engines, so we put your template engine in your template engine so you can load while you code.
$_tpl->assign('_tpl', $_tpl);

// If we've been redirected from Facebook with certain URL parameters, it's safe to assume that the user wants to login with Facebook.
if ( FLUENT_FACEBOOK ) {
	$code = $_fluent->_get('code', null);
	$error_reason = $_fluent->_get('error_reason', null);
	$error_description = $_fluent->_get('error_description', null);
	if ( $code || ( $error_reason && $error_description ) ) {
		controller('auth_facebook')->oauth_authenticate();
		die();
	}
}

function fluent_initialise() {
	global $_fluent, $_controller, $_tpl;
	$controller = FluentString::replace('-', '_', $_fluent->_get('controller', null));
	$method = FluentString::replace('-', '_', $_fluent->_get('method', null));
	$protocol = $_fluent->_server('SERVER_PROTOCOL', 'HTTP/1.0');

	if ( !$controller ) {
		header($protocol . ' 301 Moved Permanently', true, 301);
		header('Location: ' . URL . '/home');
		die();
	}

	if ( c_disabled($controller) || ( c_enabled($method) && !controller($controller)->method_exists($method) ) || ( !$method && controller($controller)->shouldReturn404() ) ) {
		header($protocol . ' 404 Not Found', true, 404);
		$controller = 'error404';
	}

	if ( controller($controller)->shouldReturn403() ) {
		header($protocol . ' 403 Forbidden', true, 403);
		$controller = 'login';
	}

	if ( $controller && $method ) {
		if ( controller($controller) && controller($controller)->method_exists($method) ) {
			controller($controller)->$method();
			die();
		} else {
			header('HTTP/1.0 404 Not Found', true, 404);
			$controller = controller('error404');
		}
	}

	$_controller = controller($controller);
	$_tpl->assign('_controller', $_controller);
}
